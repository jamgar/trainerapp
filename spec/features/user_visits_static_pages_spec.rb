require 'rails_helper'

RSpec.feature "User Visits" do
  
  scenario "home page" do
    visit '/'
    
    expect(page).to have_content("Welcome to Tri Now Endurance Online Training")
    expect(page).to have_link("Home")
    expect(page).to have_link("About")
    expect(page).to have_link("Contact")
  end
  
  scenario "about page" do
    visit '/'
    
    click_link "About"
    
    expect(page).to have_content("About")
    expect(page).to have_link("Home")
    expect(page).to have_link("About")
    expect(page).to have_link("Contact")
  end
  
  scenario "contact page" do
    visit '/'
    
    click_link "Contact"
    
    expect(page).to have_content("Contact Us")
    expect(page).to have_content("Email")
    expect(page).to have_content("Message")
    expect(page).to have_button("Send Message")
    expect(page).to have_link("Home")
    expect(page).to have_link("About")
    expect(page).to have_link("Contact")
  end  
end