class RolesController < ApplicationController
  before_filter :authenticate_user!
  # before_action :set_role, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  
  def index
    @roles = Role.all
  end
  
  def show
    if @role.users.length == 0
      @assosciated_users = "None"
    else
      @assosciated_users = @role.users.map(&:first_name).join(", ")
    end    
  end
  
  def new
    # @role = Role.new
  end
  
  def create
    # @role = Role.new(role_params)
    
    if @role.save
      flash[:success] = "The role was saved successfully"
      redirect_to roles_path
    else
      flash[:danger] = "The role did not save successfully"
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @role.update(role_params)
      flash[:success] = "The role was updated successfully"
      redirect_to roles_path
    else
      flash[:danger] = "The role did not update successfully"
      render 'edit'
    end
  end
  
  def destroy
    @role.destroy
    flash[:success] = "Role was successfully deleted."
    redirect_to roles_path
  end
  
  
  
  private
    def role_params
      params.require(:role).permit(:name, :description)
    end
    
    # def set_role
    #   @role = Role.find(params[:id])
    # end
end