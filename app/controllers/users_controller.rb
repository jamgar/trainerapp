class UsersController < ApplicationController
  before_filter :authenticate_user!
  # before_action :set_user, only: [:show, :edit, :update]
  load_and_authorize_resource
  
  def index
    @users = User.all
  end
  
  def show
    @joined_on = @user.created_at.to_formatted_s(:short)
    if @user.current_sign_in_at
      @last_login = @user.current_sign_in_at.to_formatted_s(:short)
    else
      @last_login = "never"
    end    
  end
  
  def edit
  end
  
  def update
    
    if user_params[:password].blank?
      user_params.delete(:password)
      user_params.delete(:password_confirmation)
    end
  
    successfully_updated = if needs_password?(@user, user_params)
                             @user.update(user_params)
                           else
                             @user.update_without_password(user_params)
    end    
    
    if successfully_updated
      flash[:success] = "The user was updated successfully"
      redirect_to users_path
    else
      flash[:danger] = "The user did not update successfully"
      render 'edit'
    end    
  end
  
  
  
  protected
    def needs_password?(user, params)
      params[:password].present?
    end
  
  
  private
    # def set_user
    #   @user = User.find(params[:id])
    # end
    
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :role_id)
    end

end