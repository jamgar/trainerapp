class StaticPagesController < ApplicationController
  def home
  end
  
  def about
  end
  
  def contact
    # Example to for nonRESTful methods
    authorize! :read, :contact
  end
end
