class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :role
  validates :first_name, presence: true
  validates :last_name, presence: true
  before_save :assign_role
  
  
  def assign_role
    self.role = Role.find_by name: "Guest" if self.role.nil?
  end
  
  def admin?
    self.role.name == "Admin"
  end
  
  def coach?
    self.role.name == "Coach"
  end
  
  def athlete?
    self.role.name == "Athlete"
  end
  def guest?
    self.role.name == "Guest"
  end
  
end
